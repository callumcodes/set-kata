package client

import models._
import org.scalatest.{FlatSpec, Matchers}

import scala.io.AnsiColor

class MainSpec extends FlatSpec with Matchers {

  "renderCard" should "output 0 for oval" in {
    Main.renderCard(Card(Red, One, Oval, Solid)) should include("0")
  }

  it should "output ~ for squiggle" in {
    Main.renderCard(Card(Red, One, Squiggle, Solid)) should include("~")
  }

  it should "output ♦ for diamond" in {
    Main.renderCard(Card(Red, One, Diamond, Solid)) should include("♦")
  }

  it should "output the right quantity with the rest as spaces" in {
    Main.renderCard(Card(Red, One, Oval, Solid)) should include("0  ")
    Main.renderCard(Card(Red, Two, Oval, Solid)) should include("00 ")
    Main.renderCard(Card(Red, Three, Oval, Solid)) should include("000")

    Main.renderCard(Card(Red, One, Squiggle, Solid)) should include("~  ")
    Main.renderCard(Card(Red, Two, Squiggle, Solid)) should include("~~ ")
    Main.renderCard(Card(Red, Three, Squiggle, Solid)) should include("~~~")

    Main.renderCard(Card(Red, One, Diamond, Solid)) should include("♦  ")
    Main.renderCard(Card(Red, Two, Diamond, Solid)) should include("♦♦ ")
    Main.renderCard(Card(Red, Three, Diamond, Solid)) should include("♦♦♦")
  }


  it should "output the shading as outlines" in {
    Main.renderCard(Card(Red, Two, Oval, Solid)) should include("|00 |")
    Main.renderCard(Card(Red, One, Squiggle, Striped)) should include("/~  /")
    Main.renderCard(Card(Red, Three, Diamond, Outlined)) should include("¦♦♦♦¦")
  }

  it should "assign the right colours" in {
    Main.renderCard(Card(Red, One, Squiggle, Striped)).contains(AnsiColor.RED_B) shouldBe true
    Main.renderCard(Card(Purple, One, Squiggle, Striped)).contains(AnsiColor.MAGENTA_B) shouldBe true
    Main.renderCard(Card(Green, One, Squiggle, Striped)).contains(AnsiColor.GREEN_B) shouldBe true
  }
}
