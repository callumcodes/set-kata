package game

import org.scalatest.{FlatSpec, Matchers}

class DeckSpec extends FlatSpec with Matchers {

  "the deck" should "have 81 unique cards" in {
    Deck.cards.distinct.size shouldBe 81
  }

}
