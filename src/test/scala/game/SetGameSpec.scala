package game

import models._
import org.scalatest.{FlatSpec, Matchers}

class SetGameSpec extends FlatSpec with Matchers {

  val SUT = SetGame.start(List("Alice", "Bob", "Carol"))

  "start" should "initialise a game with the player names and with starting scores of 0" in {
    SUT.players shouldBe List(
      Player("Alice", 0),
      Player("Bob", 0),
      Player("Carol", 0)
    )
  }

  it should "initialise a board of 12 shuffled cards leaving 69 in the Deck" in {
    SUT.board.size shouldBe 12
    SUT.deck.size shouldBe 69
    SUT.board should not be Deck.cards.take(12)
  }

  "deal" should "add the specified number of cards to the board and decrement that amount from the deck" in {
    (0 to 10).foreach { i =>
      SUT.deal(i).board.size shouldBe SUT.board.size + i
      SUT.deal(i).deck.size shouldBe SUT.deck.size - i
    }
  }

  "game over" should "return the player with the highest score as the winner" in {
    SUT.copy(players = List(Player("Alice", 0), Player("Bob", 2), Player("Carol", 1))).gameOver.get.name shouldBe "Bob"
    SUT.copy(players = List(Player("Alice", 2), Player("Bob", 1), Player("Carol", 3))).gameOver.get.name shouldBe "Carol"
    SUT.copy(players = List(Player("Alice", 4), Player("Bob", 1), Player("Carol", 3))).gameOver.get.name shouldBe "Alice"
  }

  it should "return nobody as the winner in the event of a tie" in {
    SUT.gameOver shouldBe None
  }

  it should "in a single player game, return no winner if there is 12 cards still on the board" in {
    val unplayableBoard = Vector(
      Card(Purple, Two, Diamond, Outlined), Card(Purple, One, Oval, Outlined), Card(Green, One, Diamond, Solid),
      Card(Purple, Three, Oval, Outlined), Card(Purple, One, Oval, Striped), Card(Red, Three, Diamond, Solid),
      Card(Red, One, Oval, Outlined), Card(Red, Three, Oval, Outlined), Card(Red, Two, Squiggle, Outlined),
      Card(Green, One, Oval, Striped), Card(Purple, Three, Diamond, Striped), Card(Green, Three, Oval, Striped))

    SetGame(List(Player("Solo", 5)), unplayableBoard, List()).gameOver shouldBe None
  }

  "move - when it the player does not chose 3 cards" should "decrement the player's score and return the card(s)" in {
    def simulateMove(choice: List[Int]) = {
      SUT.players.map { player =>
        val gameState = SUT.move(player, Set())
        gameState.players shouldBe SUT.players.updated(SUT.players.indexOf(player), player.copy(score = -1))
        gameState.board shouldBe SUT.board
        gameState.deck shouldBe SUT.deck
      }
    }

    val testCases: List[List[Int]] = List(
      List.fill(0)(1),
      List.fill(1)(1),
      List.fill(2)(1),
      List.fill(4)(1)
    )

    testCases.foreach(simulateMove)

  }

  "move - when the player has an invalid set" should "decrement the player's score and return the cards" in {
    val startingBoard = Deck.cards.take(12).toVector
    val deck = Deck.cards.drop(12)
    val fixedState = SUT.copy(board = startingBoard, deck = deck)
    fixedState.players.map { player =>
      val gameState = fixedState.move(player, Set(1, 4, 6))
      gameState.players shouldBe fixedState.players.updated(fixedState.players.indexOf(player), player.copy(score = -1))
      gameState.board shouldBe fixedState.board
      gameState.deck shouldBe fixedState.deck
    }
  }

  "move - if the player has a set" should
    "remove the chosen cards from the board and replenish then from the top of the deck" in {
    val startingBoard = Deck.cards.take(12).toVector
    val deck = Deck.cards.drop(12)
    val result = SUT.copy(board = startingBoard, deck = deck).move(SUT.players.head,
      chosenCardIndices = Set(6, 7, 8)
    )

    result.deck shouldBe deck.drop(3)
    result.board shouldBe Vector(
      startingBoard(0), startingBoard(1), startingBoard(2),
      startingBoard(3), startingBoard(4), startingBoard(5),
      deck(0), deck(1), deck(2),
      startingBoard(9), startingBoard(10), startingBoard(11)
    )
  }

  it should "just remove the cards when there is none left in the deck" in {
    val startingBoard = Deck.cards.take(12).toVector
    val deck = List()
    val result = SUT.copy(board = startingBoard, deck = deck).move(SUT.players.head,
      chosenCardIndices = Set(6, 7, 8)
    )
    result.deck shouldBe List()
    result.board shouldBe Vector(
      startingBoard(0), startingBoard(1), startingBoard(2),
      startingBoard(3), startingBoard(4), startingBoard(5),
      startingBoard(9), startingBoard(10), startingBoard(11)
    )
  }

  it should "just remove the cards if there was more than 15 cards on the board and a set is in the remaining 12" in {
    val startingBoard = Deck.cards.take(15).toVector
    val deck = Deck.cards.drop(15)
    val result = SUT.copy(board = startingBoard, deck = deck).move(SUT.players.head,
      chosenCardIndices = Set(6, 7, 8)
    )
    result.deck.size shouldBe deck.size - 3
    result.board shouldBe Vector(
      startingBoard(0), startingBoard(1), startingBoard(2),
      startingBoard(3), startingBoard(4), startingBoard(5),
      startingBoard(9), startingBoard(10), startingBoard(11),
      startingBoard(12), startingBoard(13), startingBoard(14)
    )
  }

  it should "keep playing sets of 3 cards if there no sets on the board until the deck is empty" in {
    val unplayableBoardOfFifteen = Vector(
      Card(Purple, Three, Oval, Solid), Card(Purple, One, Squiggle, Outlined), Card(Green, One, Diamond, Outlined),
      Card(Green, One, Squiggle, Outlined), Card(Red, Three, Oval, Solid), Card(Purple, One, Diamond, Striped),
      Card(Green, One, Diamond, Solid), Card(Red, Two, Oval, Striped), Card(Red, One, Diamond, Striped),
      Card(Red, Two, Oval, Solid), Card(Green, One, Squiggle, Solid), Card(Red, Three, Oval, Outlined),
      Card(Red, Three, Squiggle, Solid), Card(Red, Three, Diamond, Outlined), Card(Green, Two, Oval, Striped))

    val player = SUT.players.head

    //For the first valid move
    val aSet = Vector(Card(Red, One, Oval, Solid), Card(Red, One, Oval, Outlined), Card(Red, One, Oval, Striped))

    val unplayableGame = new SetGame(List(player), aSet ++ unplayableBoardOfFifteen.take(9), unplayableBoardOfFifteen.takeRight(6).toList)
    val unplayableResult = unplayableGame.move(player, Set(0, 1, 2))
    unplayableResult.setOnBoard shouldBe false
    unplayableResult.deck.size shouldBe 0
    unplayableResult.board.size shouldBe 15 // 9 -> 12 -> 15

    val resolvableGame = unplayableGame.copy(deck = unplayableGame.deck ++ aSet ++ aSet)
    val resolvableResult = resolvableGame.move(player, Set(0, 1, 2))
    resolvableResult.setOnBoard shouldBe true
    resolvableResult.deck.size shouldBe 3
    resolvableResult.board.size shouldBe 18 // 9 -> 12 -> 15 -> 18
  }


  "validate set" should "return true for all different cards" in {
    SUT.validateSet(List(
      Card(Red, One, Oval, Solid),
      Card(Purple, Two, Squiggle, Striped),
      Card(Green, Three, Diamond, Outlined)
    )) shouldBe true
  }

  it should "return true for all cards being the same" in {
    SUT.validateSet(List(
      Card(Red, One, Oval, Solid),
      Card(Red, One, Oval, Solid),
      Card(Red, One, Oval, Solid)
    )) shouldBe true
  }

  it should "return true for a mixture of same properties and different properties" in {
    SUT.validateSet(List(
      Card(Red, One, Oval, Solid),
      Card(Purple, One, Squiggle, Solid),
      Card(Green, One, Diamond, Solid)
    )) shouldBe true

    SUT.validateSet(List(
      Card(Red, One, Oval, Solid),
      Card(Red, Two, Oval, Striped),
      Card(Red, Three, Oval, Outlined)
    )) shouldBe true
  }

  it should "return false if any property is not unique/uniform" in {
    SUT.validateSet(List(
      Card(Red, One, Oval, Solid),
      Card(Purple, Two, Oval, Striped),
      Card(Red, Three, Oval, Outlined)
    )) shouldBe false

    SUT.validateSet(List(
      Card(Red, One, Oval, Solid),
      Card(Red, Two, Oval, Striped),
      Card(Red, Two, Oval, Outlined)
    )) shouldBe false

    SUT.validateSet(List(
      Card(Red, One, Squiggle, Solid),
      Card(Red, Two, Squiggle, Striped),
      Card(Red, Three, Oval, Outlined)
    )) shouldBe false

    SUT.validateSet(List(
      Card(Red, One, Oval, Solid),
      Card(Red, Two, Oval, Outlined),
      Card(Red, Three, Oval, Outlined)
    )) shouldBe false
  }

  "setOnBoard" should "return true when there is a set" in {
    SUT.setOnBoard shouldBe true
  }

  it should "return false when is no sets" in {
    val unplayableBoard = Vector(
      Card(Purple, Two, Diamond, Outlined), Card(Purple, One, Oval, Outlined), Card(Green, One, Diamond, Solid),
      Card(Purple, Three, Oval, Outlined), Card(Purple, One, Oval, Striped), Card(Red, Three, Diamond, Solid),
      Card(Red, One, Oval, Outlined), Card(Red, Three, Oval, Outlined), Card(Red, Two, Squiggle, Outlined),
      Card(Green, One, Oval, Striped), Card(Purple, Three, Diamond, Striped), Card(Green, Three, Oval, Striped))


    SUT.copy(board = Vector()).setOnBoard shouldBe false
    SUT.copy(board = unplayableBoard).setOnBoard shouldBe false
  }

  "noSetsFoundByUser" should "deal 3 more cards to the board" in {
    val result = SUT.noSetsFoundByUser
    result.board.size shouldBe SUT.board.size + 3
    result.deck.size shouldBe SUT.deck.size - 3
  }
}
