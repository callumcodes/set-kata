package client

import game.SetGame
import models._

import scala.annotation.tailrec
import scala.io.AnsiColor

object Main extends App with AnsiColor {


  def renderCard(card: Card): String = {
    val symbol = card.shape match {
      case Oval => "0"
      case Squiggle => "~"
      case Diamond => "♦"
    }

    val pattern = card.quantity match {
      case One => symbol + "  "
      case Two => (symbol * 2) + " "
      case Three => symbol * 3
    }

    val contents = card.shading match {
      case Solid => s"|$pattern|"
      case Striped => s"/$pattern/"
      case Outlined => s"¦$pattern¦"
    }

    card.colour match {
      case Red => s"$RED_B$contents$RESET"
      case Purple => s"$MAGENTA_B$contents$RESET"
      case Green => s"$GREEN_B$contents$RESET"
    }
  }


  def parseMove(setGame: SetGame): Option[(Player, Set[Int])] = {
    val player = if(setGame.players.size > 1) {
      println("\nYell Set? " + setGame.players.map(_.name).zipWithIndex.map {
        case (name, index) => s"$name [$index]"
      }.mkString(", "))

      val playerIndex = scala.io.StdIn.readLine().trim.toInt
      val chosenPlayer = setGame.players(playerIndex)
      println(s"${chosenPlayer.name.toUpperCase} YELLED SET!")
      chosenPlayer
    } else {
      setGame.players.head
    }
    
    val sanitisedInput =
      scala.io.StdIn.readLine("\nEnter your choices, separated by spaces. If you can't find a set use 'N'\n")
        .trim.replaceAll("""\s\s+""", " ")
    if(sanitisedInput.toLowerCase.contains("n")) {
      None
    } else {
      Some((player, sanitisedInput.split(" ").map(_.toInt).toSet))
    }
  }


  @tailrec
  def gameLoop(setGame: SetGame): SetGame = {
    // No more moves available
    if(!setGame.setOnBoard) setGame
    else {

      // Render Scores
      println("\nScores: ")
      setGame.players.foreach(p => println(s"${p.name} // ${p.score}"))
      println(s"Cards in Deck // ${setGame.deck.size}")

      //Render Board
      setGame.board.zipWithIndex.foreach {
        case (card, index) => println(s"  $index  ${renderCard(card)}")
      }

      // Take input and move
      parseMove(setGame) match {
        case Some((player, choices)) => gameLoop(setGame.move(player, choices))
        case None => gameLoop(setGame.noSetsFoundByUser)
      }
    }

  }

  println(
    """
      |              +/:::::::::::::/-     `+::::::::::::::+.      +::::::::::::::/.
      |          ````s//////////////+/  ```.s//////////////o.  ```.y//////////////o.
      |         .NNNNNNNNNNNNNNNNh++o/ :NNNNNNNNNNNNNNmNy++s. .NNNNNNmNNNNNNNNNy++o.
      |     ....-dddddddddddmNNNNh+oo/./dddddddddddmNNmNy+os:.-mddddddddddmNNNNy++s.
      |     +```````````````+NNNNh/o```````````````oNNmNy/o```````````````oNNNNy//o.
      |     +    -osoo:o    +NNNNh:o    `//::/`    oNNmNs:o    -::/::.    oNNNNy::+.
      |     +   /mm.`:mm    +NNNNh+o     hN+.y-    oNNmNy+o    y+hNos+    oNNNNy++o.
      |     +   oNN+  om    +NNNNh+o     hNo/:.    oNNmNy+o    o`hN+./    oNNNNy++s.
      |     +   .dNNs.`-    +NNNNh/o     hN+//     oNNmNy/o      hN+      oNNNNy//o.
      |     +    -hNNd:     +NNNNh:o     hN/ `     oNNmNs:o      hN+      oNNNNy::+.
      |     +     `omNm:    +NNNNh:o     hN/ +-    oNNmNs:o      hN+      oNNNNy::/`
      |     +       /mNm.   +NNNNs +    `hmo/d-    oNNmN+ +     `hNs      oNNNNo
      |     +        :mNo   /++++: +    `.....     +++/+- +     `...      +oso+-
      |     +`````````sNy```+`     +```````````````+      +```````````````+./-
      |     ....+-....sNs...-      ................-      ................-
      |         m+   `dd`
      |         mm:-/hs`
      |         /-//:`
      |
    """.stripMargin)


  println("Welcome! Please enter the number of players:")
  val numberOfPlayers = scala.io.StdIn.readInt()

  @tailrec
  def readPlayers(names: List[String], count: Int): List[String] = {
    if(count == 0) names.reverse
    else readPlayers(scala.io.StdIn.readLine(s"Enter the name of Player ${numberOfPlayers - count}\n") :: names, count - 1)
  }

  val endResult = gameLoop(SetGame.start(readPlayers(List(), numberOfPlayers)))

  endResult.gameOver match {
    case Some(Player(name, _)) => println(s"YOU WIN ${name.toUpperCase}! (•̀o•́)ง")
    case None => {
      if(endResult.players.size == 1) {
        println(s"YOU LOSE ( ≧Д≦)")
        println("Try again next time!")
      } else {
        println("It's a Draw!!! ヽ(ﾟДﾟ)ﾉ")
      }
    }

  }

}
