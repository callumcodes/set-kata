package models

trait Quantity

case object One extends Quantity

case object Two extends Quantity

case object Three extends Quantity

object Quantity {
  val all = List(One, Two, Three)
}
