package models

sealed trait Colour

case object Red extends Colour

case object Purple extends Colour

case object Green extends Colour

object Colour {
  val all = List(Red, Purple, Green)
}
