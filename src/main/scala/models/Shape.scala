package models

trait Shape

case object Oval extends Shape

case object Squiggle extends Shape

case object Diamond extends Shape


object Shape {
  val all = List(Oval, Squiggle, Diamond)
}