package models

trait Shading

case object Solid extends Shading

case object Striped extends Shading

case object Outlined extends Shading


object Shading {
  val all = List(Solid, Striped, Outlined)
}