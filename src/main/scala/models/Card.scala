package models

case class Card(colour: Colour, quantity: Quantity, shape: Shape, shading: Shading)