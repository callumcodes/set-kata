package models

case class Player(name: String, score: Int)
