package game

import models._

import scala.annotation.tailrec
import scala.util.Random

case class SetGame(players: List[Player], board: Vector[Card], deck: List[Card]) {

  import SetGame._

  private[game] def deal(numberOfCards: Int) = SetGame(players, board ++ deck.take(numberOfCards), deck.drop(numberOfCards))

  @tailrec
  private def dealUntilPlayableOrOver: SetGame =
    if (this.setOnBoard || this.deck.isEmpty) this
    else this.deal(SET_SIZE).dealUntilPlayableOrOver

  def move(player: Player, chosenCardIndices: Set[Int]): SetGame = {
    val chosenSet = chosenCardIndices.map(board(_)).toList
    if (chosenSet.size != SET_SIZE || !validateSet(chosenSet)) {
      SetGame(
        players.updated(players.indexOf(player), player.copy(score = player.score - 1)),
        board,
        deck
      )
    } else {
      val updatedGame = SetGame(
        players.updated(players.indexOf(player), player.copy(score = player.score + 1)),
        board = replaceCardsOnBoard(chosenCardIndices),
        deck = deck.drop(SET_SIZE)
      )

      updatedGame.dealUntilPlayableOrOver
    }

  }

  private def replaceCardsOnBoard(chosenCardIndices: Set[Int]): Vector[Card] = {

    lazy val replaceCards =
      chosenCardIndices.zip(deck.take(SET_SIZE))
        .foldLeft(board) { case (b, (index, card)) => b.updated(index, card) }

    lazy val removeCards =
      board.zipWithIndex
        .collect { case (card, index) if !chosenCardIndices.contains(index) => card }

    if (board.size > BOARD_SIZE) {
      removeCards
    } else {
      if (chosenCardIndices.size > deck.size) {
        removeCards
      } else {
        replaceCards
      }
    }
  }

  def validateSet(set: List[Card]): Boolean = {
    checkProperty[Colour](set.map(_.colour)) &&
      checkProperty[Quantity](set.map(_.quantity)) &&
      checkProperty[Shape](set.map(_.shape)) &&
      checkProperty[Shading](set.map(_.shading))
  }

  private def checkProperty[A](values: List[A]): Boolean = {
    val uniques: Int = values.distinct.size
    uniques == 1 || uniques == values.size
  }

  lazy val gameOver: Option[Player] = {

    if (players.size == 1) {
      if (board.size < BOARD_SIZE) Some(players.head) else None
    } else {
      val highScore = players.map(_.score).max
      val winners = players.filter(_.score == highScore)
      if (winners.size == 1) Some(winners.head) else None
    }
  }

  val setOnBoard: Boolean = {
    val cardList = board.toList
    val combinations = for {
      one <- cardList
      two <- cardList.filterNot(_ == one)
      three <- cardList.filterNot(c => c == one || c == two)
    } yield validateSet(List(one, two, three))
    combinations.fold(false)(_ || _)
  }

  lazy val noSetsFoundByUser: SetGame = deal(SET_SIZE)
}

object SetGame {

  final val SET_SIZE = 3
  final val BOARD_SIZE = 12

  private val random = new Random()

  @tailrec
  def start(names: List[String]): SetGame = {

    val newGame = SetGame(
      players = names.map(Player(_, 0)),
      board = Vector(),
      deck = random.shuffle(Deck.cards)
    ).deal(BOARD_SIZE)


    // Never start with an unsolvable board, that's just bad game design
    if(newGame.setOnBoard) newGame
    else start(names)
  }
}
