package game

import models._

object Deck {
  lazy val cards: List[Card] = for {
    colour <- Colour.all
    quantity <- Quantity.all
    shape <- Shape.all
    shading <- Shading.all
  } yield Card(colour, quantity, shape, shading)
}
