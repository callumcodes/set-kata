# Set Card Game Kata

A kata that implements the game Set.
The rules are available [online](https://www.setgame.com/sites/default/files/instructions/SET%20INSTRUCTIONS%20-%20ENGLISH.pdf)

## What is it?

This is an implementation of the card game Set. `SetGame` contains all of the game logic.
There is a simple console application provided to demonstrate an implementation with the game class and to show the game logic working outside of unit tests.

## How to play

* Start the game with `sbt run`. Enter the number of players and their names.
* If in a multiplier game enter the number of the player calling "SET!" as shown
* Enter the numbers of the cards of your set seperated by space
* The game is over when there is no cards in the deck and no sets on the board, highest score wins
* In single player you must finish with less than 12 cards on the board otherwise you lose. 


*NOTE: The game knows if there is a set on the board and will keep dealing cards until there is. If you cannot find the present set, you can deal more cards using 'n' when taking a move*





## Acceptance Criteria

* Run a single player move - ```SetGame.move``` takes in a player and their list of card indices from the board and carries out the game logic
* Return the current players' scores - ```SetGame.players``` contains the players and their scores
* Identify who has won a game - ```SetGame.gameOver``` will return the player with the highest score as the winner

## Assumptions made

* The official rules do not specify what occurs in the event of a draw in multiplayer. In this implementation nobody wins.
* Deciding if the board contains any sets can be determined by the program, removing this element from the player(s) improves the pace of the game. If the users can not find the present set on the board they can still deal more cards.
* The deck should be a `List` as we only access the top
* The board should be a `Vector` as they have better performance with random access

## Running the tests

To run the tests use
```bash
sbt test
```

## Licence
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
